const express = require('express');
const path = require('path');
const fs = require('fs');
const uuid = require('uuid');

const app = express();
const port = process.env.PORT || 7000;


app.get('/html', (req, res) => {
    res.status(200).sendFile(path.join(__dirname, 'index.html'), (err) => {
        if (err) {
            res.status(500).json({ msg: "no file found" }).end();
        } else {
            res.end();
        }
    });

})
app.get('/json', (req, res) => {

    res.status(200).sendFile(path.join(__dirname, 'data1.json'), (err) => {
        if (err) {
            res.status(500).json({ msg: "no file found" }).end();
        }
        else {
            res.end();
        }
    });

});

app.get('/uuid', (req, res) => {
    res.status(200).json({ uuid: uuid.v4() }).end();
})


app.get('/status/:status_code', (req, res) => {

    try {
        res.sendStatus(parseInt(req.params.status_code)).end();
    }
    catch (err) {
        res.status(400).json({ msg: "invalid status code" }).end();
    }

})


app.get('/delay/:delay_in_sec', (req, res) => {
    if (Number.isInteger(parseInt(req.params.delay_in_sec)) && parseInt(req.params.delay_in_sec) >= 0) {
        setTimeout(() => {
            res.sendStatus(200).end();
        }, (req.params.delay_in_sec) * 1000);
    }
    else {
        res.status(400).json({ msg: "bad request" }).end();
    }
})



//listening
app.listen(port, () => console.log(`server started at port ${port}`));




